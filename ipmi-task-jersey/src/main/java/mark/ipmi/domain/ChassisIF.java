package mark.ipmi.domain;

import mark.ipmi.web.markup.Export;

/**
 * Defines functionality related to Chassis
 * 
 * @author mark
 *
 */
public interface ChassisIF {
	
	@Export(name="isPowerOn")
	public boolean isPowerOn();

}
