package mark.ipmi.domain;

/**
 * Specifies necessary data for authentication on IPMI server
 * 
 * @author mark
 * 
 */
public interface IpmiCredentialsIF {

	public String getHost();

	public String getLogin();

	public String getPassword();

}
