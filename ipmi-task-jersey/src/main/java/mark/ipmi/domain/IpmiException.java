package mark.ipmi.domain;

/**
 * base exception for IPMI business logic
 * 
 * @author mark
 * 
 */
public class IpmiException extends Exception {

	private static final long serialVersionUID = 1L;

	public IpmiException() {
	}

	public IpmiException(String message) {
		super(message);
	}

	public IpmiException(Throwable cause) {
		super(cause);
	}

	public IpmiException(String message, Throwable cause) {
		super(message, cause);
	}

	public IpmiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
