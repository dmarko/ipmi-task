package mark.ipmi.domain.verax;

import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatusResponseData;

import mark.ipmi.domain.ChassisIF;

/**
 * Concrete implementation of {@link ChassisIF}, which uses Verax IPMI library
 * 
 * @author mark
 * 
 */
public class VeraxChassis implements ChassisIF {

	private boolean isPowerOn;

	@Override
	public boolean isPowerOn() {
		return isPowerOn;
	}

	public VeraxChassis(GetChassisStatusResponseData getChassisStatusResponseData) {
		isPowerOn = getChassisStatusResponseData.isPowerOn();
	}

}
