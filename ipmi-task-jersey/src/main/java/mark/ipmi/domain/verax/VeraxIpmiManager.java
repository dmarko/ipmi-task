package mark.ipmi.domain.verax;

import java.net.InetAddress;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.IpmiManagerIF;
import mark.ipmi.domain.SelEntryIF;
import mark.ipmi.domain.verax.commands.DeleteSel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.veraxsystems.vxipmi.api.async.ConnectionHandle;
import com.veraxsystems.vxipmi.api.sync.IpmiConnector;
import com.veraxsystems.vxipmi.coding.commands.IpmiCommandCoder;
import com.veraxsystems.vxipmi.coding.commands.IpmiVersion;
import com.veraxsystems.vxipmi.coding.commands.PrivilegeLevel;
import com.veraxsystems.vxipmi.coding.commands.chassis.ChassisControl;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatus;
import com.veraxsystems.vxipmi.coding.commands.chassis.GetChassisStatusResponseData;
import com.veraxsystems.vxipmi.coding.commands.chassis.PowerCommand;
import com.veraxsystems.vxipmi.coding.commands.sel.GetSelEntry;
import com.veraxsystems.vxipmi.coding.commands.sel.GetSelEntryResponseData;
import com.veraxsystems.vxipmi.coding.protocol.AuthenticationType;
import com.veraxsystems.vxipmi.coding.security.CipherSuite;

/**
 * Concrete implementation of {@link IpmiManagerIF}, which uses Verax IPMI
 * library
 * 
 * @author mark
 * 
 */
public class VeraxIpmiManager implements IpmiManagerIF {

	private final static Logger logger = LoggerFactory.getLogger(VeraxIpmiManager.class);

	private IpmiVersion ipmiVersion = IpmiVersion.V20;
	private AuthenticationType authenticationType = AuthenticationType.RMCPPlus;

	@Override
	public SelEntryIF getFirstSelEntry(IpmiCredentialsIF credentials) throws IpmiException {
		return getSelEntry(0, credentials);
	}

	@Override
	public SelEntryIF getLastSelEntry(IpmiCredentialsIF credentials) throws IpmiException {
		return getSelEntry(0xFFFF, credentials);
	}

	@Override
	public SelEntryIF getSelEntry(int id, IpmiCredentialsIF credentials) throws IpmiException {
		VeraxSelEntry result = null;

		GetSelEntry request = new GetSelEntry(ipmiVersion, null, authenticationType, 0, id);
		GetSelEntryResponseData response = (GetSelEntryResponseData) call(request, credentials);
		result = new VeraxSelEntry(response);

		return result;
	}

	@Override
	public ChassisIF getChassis(IpmiCredentialsIF credentials) throws IpmiException {
		VeraxChassis result = null;

		GetChassisStatus request = new GetChassisStatus(ipmiVersion, null, authenticationType);
		GetChassisStatusResponseData response = (GetChassisStatusResponseData) call(request, credentials);
		result = new VeraxChassis(response);

		return result;
	}

	@Override
	public void switchPower(boolean on, IpmiCredentialsIF credentials) throws IpmiException {
		ChassisIF chassis = getChassis(credentials);
		if (on && chassis.isPowerOn())
			throw new IpmiException("Chassis power is already on");
		if (!on && !chassis.isPowerOn())
			throw new IpmiException("Chassis power is already off");

		ChassisControl request = new ChassisControl(IpmiVersion.V20, null, AuthenticationType.RMCPPlus, on ? PowerCommand.PowerUp : PowerCommand.PowerDown);
		call(request, credentials);

	}

	@Override
	public void resetPower(IpmiCredentialsIF credentials) throws IpmiException {
		ChassisControl request = new ChassisControl(IpmiVersion.V20, null, AuthenticationType.RMCPPlus, PowerCommand.HardReset);
		call(request, credentials);
	}

	private Object call(IpmiCommandCoder request, IpmiCredentialsIF credentials) throws IpmiException {

		Object response = null;

		IpmiConnector connector = null;
		try {
			connector = new IpmiConnector(0);
			logger.debug("Connector created");

			ConnectionHandle handle = connector.createConnection(InetAddress.getByName(credentials.getHost()));
			logger.debug("Connection created");

			CipherSuite cs = connector.getAvailableCipherSuites(handle).get(3);
			logger.debug("Cipher suite picked");

			connector.getChannelAuthenticationCapabilities(handle, cs, PrivilegeLevel.Administrator);
			logger.debug("Channel authentication capabilities receivied");

			connector.openSession(handle, credentials.getLogin(), credentials.getPassword(), null);
			logger.debug("Session open");

			request.setCipherSuite(cs);
			response = connector.sendMessage(handle, request);

			connector.closeSession(handle);
			logger.debug("Session closed");

		} catch (Exception e) {
			throw new IpmiException(e);
		} finally {
			if (connector != null)
				connector.tearDown();
			logger.debug("Connection manager closed");
		}

		return response;

	}

	@Override
	public void addSelEntry(SelEntryIF likeEntry, IpmiCredentialsIF credentials) throws IpmiException {
		throw new IpmiException("not implmented");
	}

	@Override
	public void deleteSelEntry(int id, IpmiCredentialsIF credentials) throws IpmiException {
		DeleteSel request = new DeleteSel(IpmiVersion.V20, null, AuthenticationType.RMCPPlus, 0, id);
		call(request, credentials);
	}

	@Override
	public void deleteFirstSelEntry(IpmiCredentialsIF credentials) throws IpmiException {

		deleteSelEntry(0, credentials);
	}

	@Override
	public void deleteLastSelEntry(IpmiCredentialsIF credentials) throws IpmiException {

		deleteSelEntry(0xFFFF, credentials);
	}

}
