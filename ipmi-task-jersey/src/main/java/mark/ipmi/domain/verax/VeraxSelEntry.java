package mark.ipmi.domain.verax;

import java.util.Date;

import mark.ipmi.domain.SelEntryIF;

import com.veraxsystems.vxipmi.coding.commands.sel.GetSelEntryResponseData;

/**
 * Concrete implementation of {@link SelEntryIF}, which uses Verax IPMI library
 * 
 * @author mark
 *
 */
public class VeraxSelEntry implements SelEntryIF {
	
	private int id;
	private VeraxSelEntry nextSelEntry;
	private Date timestamp;
	private int type;
	
	public VeraxSelEntry(GetSelEntryResponseData getSelEntryResponseData) {
		super();
		this.id = getSelEntryResponseData.getSelRecord().getRecordId();
		this.timestamp = getSelEntryResponseData.getSelRecord().getTimestamp();
		this.type = getSelEntryResponseData.getSelRecord().getRecordType().getCode();
		if(getSelEntryResponseData.getNextRecordId()!=0xffff) this.nextSelEntry = new VeraxSelEntry(getSelEntryResponseData.getNextRecordId());
	}
	public VeraxSelEntry(int id) {
		super();
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public SelEntryIF getNext() {
		return nextSelEntry;
	}

	@Override
	public Date getTimestamp() {
		return timestamp;
	}
	@Override
	public int getType() {
		return type;
	}

}
