package mark.ipmi.jaxrs.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.IpmiManagerIF;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.service.ResultData;

@Path("ipmi/chassis")
public class ChassisService implements ChassisServiceIF {

	private IpmiManagerIF ipmiManager;

	public ChassisService(IpmiManagerIF ipmiManager) {
		super();
		this.ipmiManager = ipmiManager;
	}

	/**
	 * 
	 */
	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("")
	public ResultData<ChassisIF> getChassis(@QueryParam("host") String host, @QueryParam("login") String login, @QueryParam("password") String password) {

		ResultData<ChassisIF> result;

		try {
			ChassisIF chassis = ipmiManager.getChassis(toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(chassis);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	/**
	 * 
	 */
	@Override
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("power")
	public ResultData<?> switchPower(@QueryParam("on") String on, @QueryParam("host") String host, @QueryParam("login") String login, @QueryParam("password") String password) {

		ResultData<?> result;

		try {
			ipmiManager.switchPower("true".equals(on), toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(null);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	private IpmiCredentialsIF toIpmiCredentials(final String host, final String login, final String password) {
		return new IpmiCredentialsIF() {

			@Override
			public String getPassword() {
				return password;
			}

			@Override
			public String getLogin() {
				return login;
			}

			@Override
			public String getHost() {
				return host;
			}
		};
	}

}
