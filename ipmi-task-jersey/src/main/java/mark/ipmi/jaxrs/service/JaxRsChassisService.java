package mark.ipmi.jaxrs.service;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.IpmiManagerIF;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.service.ResultData;

@Path(JaxRsChassisService.PATH_GLOBAL)
public class JaxRsChassisService implements ChassisServiceIF {

	private IpmiManagerIF ipmiManager;

	public JaxRsChassisService(IpmiManagerIF ipmiManager) {
		super();
		this.ipmiManager = ipmiManager;
	}
	
	final static String PARAM_HOST = "host";
	final static String PARAM_LOGIN = "login";
	final static String PARAM_PASSWORD = "password";
	final static String PARAM_ON = "on";
	final static String PARAM_ACTIVATED = "activated";
	final static String PATH_GLOBAL = "ipmi/chassis";
	final static String PATH_CHASSIS = "";
	final static String PATH_CHASSIS_POWER = "power";
	final static String PATH_CHASSIS_POWER_RESET = "power/reset";
	

	/**
	 * 
	 */
	@Override
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(PATH_CHASSIS)
	public ResultData<ChassisIF> getChassis(@QueryParam(PARAM_HOST) String host, @QueryParam(PARAM_LOGIN) String login, @QueryParam(PARAM_PASSWORD) String password) {

		ResultData<ChassisIF> result;

		try {
			ChassisIF chassis = ipmiManager.getChassis(toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(chassis);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	/**
	 * 
	 */
	@Override
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path(PATH_CHASSIS_POWER)
	public ResultData<?> switchPower(@FormParam(PARAM_ON) String on, @QueryParam(PARAM_HOST) String host, @QueryParam(PARAM_LOGIN) String login, @QueryParam(PARAM_PASSWORD) String password) {

		ResultData<?> result;

		try {
			ipmiManager.switchPower(Boolean.TRUE.toString().equals(on), toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(null);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	/**
	 * 
	 */
	@Override
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path(PATH_CHASSIS_POWER_RESET)
	public ResultData<?> resetPower(@FormParam(PARAM_ACTIVATED) String activated, @QueryParam(PARAM_HOST) String host, @QueryParam(PARAM_LOGIN) String login, @QueryParam(PARAM_PASSWORD) String password) {

		ResultData<?> result;

		try {
			ipmiManager.resetPower(toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(null);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	private IpmiCredentialsIF toIpmiCredentials(final String host, final String login, final String password) {
		return new IpmiCredentialsIF() {

			@Override
			public String getPassword() {
				return password;
			}

			@Override
			public String getLogin() {
				return login;
			}

			@Override
			public String getHost() {
				return host;
			}
		};
	}

}
