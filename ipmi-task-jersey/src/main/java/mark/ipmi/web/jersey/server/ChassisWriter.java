package mark.ipmi.web.jersey.server;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.service.ResultData;
import mark.ipmi.web.markup.MarkupBuilderIF;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class ChassisWriter implements MessageBodyWriter<ResultData<ChassisIF>> {

	private MarkupBuilderIF markupBuilder;

	public ChassisWriter(MarkupBuilderIF markupBuilder) {

		this.markupBuilder = markupBuilder;

	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return true;
	}

	@Override
	public long getSize(ResultData<ChassisIF> t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return -1;
	}

	@Override
	public void writeTo(ResultData<ChassisIF> t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {

		String jsonString = markupBuilder.makeMarkupString(t, type);
		PrintStream ps = new PrintStream(entityStream);
		ps.print(jsonString);

	}

}
