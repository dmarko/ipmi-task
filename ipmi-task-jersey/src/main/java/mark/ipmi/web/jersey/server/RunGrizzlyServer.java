package mark.ipmi.web.jersey.server;

import mark.ipmi.domain.verax.VeraxIpmiManager;
import mark.ipmi.jaxrs.service.JaxRsChassisService;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.web.markup.MarkupBuilderIF;
import mark.ipmi.web.markup.json.gson.GsonBasedJsonBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * Run class.
 * 
 */
public class RunGrizzlyServer {
	// Base URI the Grizzly HTTP server will listen on
	public static final String BASE_URI = "http://localhost:8080/ipmi/";

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 * 
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer() {

		// setup dependencies
		VeraxIpmiManager ipmiManager = new VeraxIpmiManager();
		ChassisServiceIF chassisService = new JaxRsChassisService(ipmiManager);

		MarkupBuilderIF markupBuilder = new GsonBasedJsonBuilder();
		ChassisWriter chassisWriter = new ChassisWriter(markupBuilder);

		// config of server
		final ResourceConfig rc = new ResourceConfig().registerInstances(chassisService, chassisWriter);

		// start server
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		final HttpServer server = startServer();
		System.out.println(String.format("Jersey app started with WADL available at " + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
		System.in.read();
		server.stop();
	}
}
