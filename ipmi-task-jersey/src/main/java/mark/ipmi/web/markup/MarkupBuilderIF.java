package mark.ipmi.web.markup;

import java.lang.reflect.Type;

import mark.ipmi.service.ResultData;

/**
 * Defines method for generating markup out of object graph
 * 
 * @author mark
 * 
 */
public interface MarkupBuilderIF {

	/**
	 * Generates markup
	 * 
	 * @param object
	 *            source data
	 * @param typeOfData
	 *            type of data (to check which methods to use)
	 * @return markup string
	 */
	public String makeMarkupString(ResultData<?> object, Type typeOfData);

}
