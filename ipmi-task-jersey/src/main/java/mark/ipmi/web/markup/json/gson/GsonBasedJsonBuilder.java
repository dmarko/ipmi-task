package mark.ipmi.web.markup.json.gson;

import java.lang.reflect.Type;

import mark.ipmi.service.ResultData;
import mark.ipmi.web.markup.MarkupBuilderIF;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Markup builder which generates JSON string outt of provided object graph and
 * uses GSON library.
 * 
 * @author mark
 * 
 */
public class GsonBasedJsonBuilder implements MarkupBuilderIF {

	private GsonBuilder gsonBuilder = null;

	public GsonBasedJsonBuilder() {
		gsonBuilder = new GsonBuilder();
		gsonBuilder.setPrettyPrinting();
	}

	public GsonBuilder getGsonBuilder() {
		return gsonBuilder;
	}

	@Override
	public String makeMarkupString(ResultData<?> object, Type typeOfData) {
		Gson gson = gsonBuilder.create();

		if (typeOfData == null)
			return gson.toJson(object);
		return gson.toJson(object, typeOfData);
	}

}
