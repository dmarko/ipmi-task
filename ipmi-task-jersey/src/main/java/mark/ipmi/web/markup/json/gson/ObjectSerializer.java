package mark.ipmi.web.markup.json.gson;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

import mark.ipmi.web.markup.Export;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

/**
 * Special serializer for GSON JSON library, which extends GSON serializing
 * functionality by using {@link Export} annotations.
 * 
 * @author mark
 * 
 */
public class ObjectSerializer implements JsonSerializer<Object> {
	private final static Logger logger = LoggerFactory.getLogger(ObjectSerializer.class);

	private static ThreadLocal<ExportFilter> exportFilterThreadLocal = new ThreadLocal<ExportFilter>();

	@SuppressWarnings("unchecked")
	@Override
	public JsonElement serialize(Object src, Type typeOfSrc, JsonSerializationContext context) {

		ExportFilter exportFilter = exportFilterThreadLocal.get();
		exportFilterThreadLocal.remove();
		if (exportFilter == null)
			exportFilter = new ExportFilter();

		TypeToken<?> typeToken = TypeToken.get(typeOfSrc);
		Class<?> srcClass = typeToken.getRawType();

		JsonObject jsonObject = new JsonObject();

		for (Method m : srcClass.getMethods()) {
			Export export = m.getAnnotation(Export.class);
			if (export == null)
				continue;

			// export filter filtering logic
			if (!exportFilter.allowed(src, export.name())) {
				continue;
			}

			Type returnType = m.getGenericReturnType();
			Object returnValue = null;
			Class<Object> formatterClass = export.formatterClass();
			Method formatterMethod = null;
			try {
				formatterMethod = formatterClass.getMethod("format", Object.class);
			} catch (Exception e) {
			}
			try {
				returnValue = m.invoke(src);
			} catch (Exception e) {
				logger.error("unable to invoke method", e);
				continue;
			}

			if (returnValue != null && formatterMethod != null) {
				try {
					returnValue = formatterMethod.invoke(formatterClass, returnValue);
					returnType = returnValue.getClass();
				} catch (Exception e) {
					logger.error("unable to invoke formatter method", e);
					continue;
				}
			}

			// new export filter
			ExportFilter newExportFilter = new ExportFilter(returnValue, export.exportFilter());
			exportFilterThreadLocal.set(newExportFilter);

			JsonElement element = context.serialize(returnValue, returnType);
			jsonObject.add(export.name(), element);
		}

		return jsonObject;
	}

	static class ExportFilter {

		public ExportFilter(Object object, String[] fieldNames) {
			this.object = object;
			this.fieldNames = fieldNames;
		}

		public ExportFilter() {
			this(null, null);
		}

		Object object;
		String[] fieldNames;

		boolean allowed(Object src, String fieldName) {
			boolean result = true;

			if (object != null && object == src && fieldNames != null && fieldNames.length > 0) {
				result = false;
				for (String fn : fieldNames) {

					result = (result || (fn.equals(fieldName)));
				}
			}

			return result;
		}

	}

}
