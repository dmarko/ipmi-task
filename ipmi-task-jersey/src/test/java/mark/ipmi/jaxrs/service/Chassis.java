package mark.ipmi.jaxrs.service;

import mark.ipmi.domain.ChassisIF;

public class Chassis implements ChassisIF {

	private boolean isPowerOn;

	@Override
	public boolean isPowerOn() {
		return isPowerOn;
	}

	public Chassis(boolean isPowerOn) {
		this.isPowerOn = isPowerOn;
	}

}
