package mark.ipmi.jaxrs.service;

import java.net.URI;
import java.util.Comparator;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;

import junit.framework.Assert;
import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.IpmiManagerIF;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.web.jersey.server.ChassisWriter;
import mark.ipmi.web.markup.MarkupBuilderIF;
import mark.ipmi.web.markup.json.gson.GsonBasedJsonBuilder;

import org.easymock.EasyMock;
import org.easymock.LogicalOperator;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ChassisServerTest {

	private HttpServer server;
	private WebTarget target;

	public static final String BASE_URI = "http://localhost:8080/ipmi/";
	private IpmiManagerIF ipmiManagerMock;

	@Before
	public void setUp() throws Exception {

		ipmiManagerMock = EasyMock.createMock(IpmiManagerIF.class);

		ChassisServiceIF chassisService = new JaxRsChassisService(ipmiManagerMock);

		MarkupBuilderIF markupBuilder = new GsonBasedJsonBuilder();
		ChassisWriter chassisWriter = new ChassisWriter(markupBuilder);

		final ResourceConfig rc = new ResourceConfig().registerInstances(chassisService, chassisWriter);

		// server
		server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);

		// client
		Client c = ClientBuilder.newClient();

		target = c.target(BASE_URI);
	}

	@After
	public void tearDown() throws Exception {
		server.stop();
	}

	/**
	 * Test to see if getChassis is properly configured ok to be accessed via
	 * rest path and params and return result in necessary format. This test
	 * checks if method returns successful result and expected data..
	 */
	@Test
	public void testGetChassisSuccess() {

		// setup expected data
		boolean expectedSuccess = true;
		boolean expectedChassisPowerOn = true;
		ChassisIF expectedChassis = new Chassis(expectedChassisPowerOn);

		// setup mock dependencies
		try {
			EasyMock.expect(ipmiManagerMock.getChassis(EasyMock.cmp(IPMI_CREDENTIALS, IPMI_CREDENTIALS_COMPARATOR, LogicalOperator.EQUAL))).andReturn(expectedChassis);
		} catch (IpmiException e) {
			Assert.fail("should not throw exception");
		}

		EasyMock.replay(ipmiManagerMock);

		// access unit under test
		String responseMsg = target.path(toPath(JaxRsChassisService.PATH_CHASSIS)).queryParam(JaxRsChassisService.PARAM_HOST, "host").queryParam(JaxRsChassisService.PARAM_LOGIN, "login").queryParam(JaxRsChassisService.PARAM_PASSWORD, "password").request().get(String.class);

		// verify

		EasyMock.verify(ipmiManagerMock);

		JsonObject resultJO = toJson(responseMsg);

		boolean actualSuccess = resultJO.getAsJsonObject().get("success").getAsBoolean();
		Assert.assertEquals(expectedSuccess, actualSuccess);

		boolean actualChasissPowerOn = resultJO.getAsJsonObject().get("data").getAsJsonObject().get("isPowerOn").getAsBoolean();
		Assert.assertEquals(expectedChassisPowerOn, actualChasissPowerOn);
	}

	/**
	 * Test to see if getChassis is properly configured ok to be accessed via
	 * rest path and params and return result in necessary format. This test
	 * checks if method returns fail result and expected reason.
	 */
	@Test
	public void testGetChassisFail() {

		// setup expected data
		boolean expectedSuccess = false;
		String expectedErrorMessage = "exception 123";
		IpmiException expectedException = new IpmiException(expectedErrorMessage);

		// setup mock dependencies
		try {
			EasyMock.expect(ipmiManagerMock.getChassis(EasyMock.cmp(IPMI_CREDENTIALS, IPMI_CREDENTIALS_COMPARATOR, LogicalOperator.EQUAL))).andThrow(expectedException);
		} catch (IpmiException e) {
			Assert.fail("should not throw exception");
		}

		EasyMock.replay(ipmiManagerMock);

		// access unit under test
		String responseMsg = target.path(toPath(JaxRsChassisService.PATH_CHASSIS)).queryParam(JaxRsChassisService.PARAM_HOST, "host").queryParam(JaxRsChassisService.PARAM_LOGIN, "login").queryParam(JaxRsChassisService.PARAM_PASSWORD, "password").request().get(String.class);

		// verify

		EasyMock.verify(ipmiManagerMock);

		JsonObject resultJO = toJson(responseMsg);

		boolean actualSuccess = resultJO.getAsJsonObject().get("success").getAsBoolean();
		Assert.assertEquals(expectedSuccess, actualSuccess);

		String actualErrorMessage = resultJO.getAsJsonObject().get("error").getAsString();
		Assert.assertEquals(expectedErrorMessage, actualErrorMessage);

	}

	/**
	 * Test to see if switchPower gets params properly and check if the power is
	 * not on and switches it successfully on.
	 */
	@Test
	public void testSwitchPowerSuccess() {

		// setup expected data
		boolean expectedSuccess = true;

		// setup mock dependencies
		try {
			ipmiManagerMock.switchPower(EasyMock.eq(true), EasyMock.cmp(IPMI_CREDENTIALS, IPMI_CREDENTIALS_COMPARATOR, LogicalOperator.EQUAL));
		} catch (IpmiException e) {
			Assert.fail("should not throw exception");
		}

		EasyMock.replay(ipmiManagerMock);

		// access unit under test
		String responseMsg = target.path(toPath(JaxRsChassisService.PATH_CHASSIS_POWER)).queryParam(JaxRsChassisService.PARAM_HOST, "host").queryParam(JaxRsChassisService.PARAM_LOGIN, "login").queryParam(JaxRsChassisService.PARAM_PASSWORD, "password").request().post(toPostEntity(JaxRsChassisService.PARAM_ON, Boolean.TRUE.toString()), String.class);

		// verify

		EasyMock.verify(ipmiManagerMock);

		JsonObject resultJO = toJson(responseMsg);

		boolean actualSuccess = resultJO.getAsJsonObject().get("success").getAsBoolean();
		Assert.assertEquals(expectedSuccess, actualSuccess);
	}

	/**
	 * Test to see if switchPower gets params properly and check if the power is
	 * not on and switches it successfully on.
	 */
	@Test
	public void testResetPowerSuccess() {

		// setup expected data
		boolean expectedSuccess = true;

		// setup mock dependencies
		try {
			ipmiManagerMock.resetPower(EasyMock.cmp(IPMI_CREDENTIALS, IPMI_CREDENTIALS_COMPARATOR, LogicalOperator.EQUAL));
		} catch (IpmiException e) {
			Assert.fail("should not throw exception");
		}

		EasyMock.replay(ipmiManagerMock);

		// access unit under test
		String responseMsg = target.path(toPath(JaxRsChassisService.PATH_CHASSIS_POWER_RESET)).queryParam(JaxRsChassisService.PARAM_HOST, "host").queryParam(JaxRsChassisService.PARAM_LOGIN, "login").queryParam(JaxRsChassisService.PARAM_PASSWORD, "password").request().post(toPostEntity(JaxRsChassisService.PARAM_ACTIVATED, Boolean.TRUE.toString()), String.class);

		// verify

		EasyMock.verify(ipmiManagerMock);

		JsonObject resultJO = toJson(responseMsg);

		boolean actualSuccess = resultJO.getAsJsonObject().get("success").getAsBoolean();
		Assert.assertEquals(expectedSuccess, actualSuccess);
	}

	private static IpmiCredentialsIF IPMI_CREDENTIALS = new IpmiCredentialsIF() {

		@Override
		public String getPassword() {
			return "password";
		}

		@Override
		public String getLogin() {
			return "login";
		}

		@Override
		public String getHost() {
			return "host";
		}
	};

	private static Comparator<IpmiCredentialsIF> IPMI_CREDENTIALS_COMPARATOR = new Comparator<IpmiCredentialsIF>() {
		@Override
		public int compare(IpmiCredentialsIF o1, IpmiCredentialsIF o2) {
			return o1.getHost().equals(o2.getHost()) && o1.getLogin().equals(o2.getLogin()) && o1.getPassword().equals(o2.getPassword()) ? 0 : -1;
		}
	};

	private static JsonObject toJson(String jsonString) {
		JsonParser jsonParser = new JsonParser();
		JsonElement element = jsonParser.parse(jsonString);
		return element.getAsJsonObject();

	}

	private static Entity<Form> toPostEntity(String key, String value) {

		Form f = new Form();
		f.param(key, value);
		return Entity.form(f);
	}

	private static String toPath(String postFix) {
		return JaxRsChassisService.PATH_GLOBAL + "/" + postFix;
	}

}
