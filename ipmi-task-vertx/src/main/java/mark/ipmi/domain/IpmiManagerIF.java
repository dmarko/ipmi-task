package mark.ipmi.domain;

/**
 * Defines all management functionality for IPMI business logic
 * 
 * @author mark
 * 
 */
public interface IpmiManagerIF {

	/**
	 * @param credentials
	 * @return first log entry
	 * @throws IpmiException
	 */

	public SelEntryIF getFirstSelEntry(IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * @return last SEL entry
	 * @throws IpmiException
	 */
	public SelEntryIF getLastSelEntry(IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * @param id
	 * @return SEL entry with specified id
	 * @throws IpmiException
	 */
	public SelEntryIF getSelEntry(int id, IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Adds new SEL entry according provided example
	 * 
	 * @param likeEntry
	 *            example according which new SEL entry will be created and
	 *            added
	 * @param credentials
	 * @throws IpmiException
	 */
	public void addSelEntry(SelEntryIF likeEntry, IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Deletes SEL entry with specified id
	 * 
	 * @param id
	 * @param credentials
	 * @throws IpmiException
	 */
	public void deleteSelEntry(int id, IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Deletes first SEL entry
	 * 
	 * @param credentials
	 * @throws IpmiException
	 */
	public void deleteFirstSelEntry(IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Deletes last SEL entry
	 * 
	 * @param credentials
	 * @throws IpmiException
	 */
	public void deleteLastSelEntry(IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * @param credentials
	 * @return Chassis object
	 * @throws IpmiException
	 */
	public ChassisIF getChassis(IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Switches power on chassis
	 * 
	 * @param on
	 *            defines whether power should be switched on or off. True means
	 *            on, False means off.
	 * @param credentials
	 * @throws IpmiException
	 */
	public void switchPower(boolean on, IpmiCredentialsIF credentials) throws IpmiException;

	/**
	 * Resets power on chassis
	 * 
	 * 
	 * @param credentials
	 * @throws IpmiException
	 */
	public void resetPower(IpmiCredentialsIF credentials) throws IpmiException;

}
