package mark.ipmi.domain;

import java.util.Date;

import mark.ipmi.web.markup.Export;

/**
 * Interface which defines functionality for SEL entry record.
 * 
 * @author mark
 * 
 */
public interface SelEntryIF {

	@Export(name = "id")
	public int getId();

	@Export(name = "next", exportFilter = { "id" })
	public SelEntryIF getNext();

	@Export(name = "timestamp")
	public Date getTimestamp();

	@Export(name = "type")
	public int getType();

}
