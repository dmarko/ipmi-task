package mark.ipmi.service;

import mark.ipmi.domain.ChassisIF;

public interface ChassisServiceIF {

	public abstract ResultData<ChassisIF> getChassis(String host, String login, String password) throws Exception;

	public abstract ResultData<?> switchPower(String on, String host, String login, String password) throws Exception;
	
	public abstract ResultData<?> resetPower(String activated, String host, String login, String password) throws Exception;

}