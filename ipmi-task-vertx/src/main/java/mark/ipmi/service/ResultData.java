package mark.ipmi.service;

/**
 * Result data container, which assembles all result which is expected by
 * client.
 * 
 * @author mark
 * 
 * @param <DATA>
 */
public class ResultData<DATA> {

	public static <T> ResultData<T> create(T data) {
		ResultData<T> rd = new ResultData<T>();
		rd.data = data;
		return rd;
	}

	public static <T> ResultData<T> createSuccess(T data) {
		ResultData<T> rd = new ResultData<T>();
		rd.data = data;
		rd.success = true;
		return rd;
	}

	public static <T> ResultData<T> createFail(Exception e) {
		ResultData<T> rd = new ResultData<T>();
		rd.error = e.getMessage();
		rd.success = false;
		return rd;
	}

	public DATA data;
	public boolean success = true;
	public String error;

}
