package mark.ipmi.vertx.service;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.IpmiManagerIF;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.service.ResultData;

public class BasicChassisService implements ChassisServiceIF {

	private IpmiManagerIF ipmiManager;

	public BasicChassisService(IpmiManagerIF ipmiManager) {
		super();
		this.ipmiManager = ipmiManager;
	}

	/**
	 * 
	 */
	@Override
	public ResultData<ChassisIF> getChassis(String host, String login, String password) {

		ResultData<ChassisIF> result;

		try {
			ChassisIF chassis = ipmiManager.getChassis(toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(chassis);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	/**
	 * 
	 */
	@Override
	public ResultData<?> switchPower(String on, String host, String login, String password) {

		ResultData<?> result;

		try {
			ipmiManager.switchPower("true".equals(on), toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(null);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;

	}

	private IpmiCredentialsIF toIpmiCredentials(final String host, final String login, final String password) {
		return new IpmiCredentialsIF() {

			@Override
			public String getPassword() {
				return password;
			}

			@Override
			public String getLogin() {
				return login;
			}

			@Override
			public String getHost() {
				return host;
			}
		};
	}

	@Override
	public ResultData<?> resetPower(String activated, String host, String login, String password) throws Exception {
		ResultData<?> result;

		try {
			ipmiManager.resetPower(toIpmiCredentials(host, login, password));
			result = ResultData.createSuccess(null);
		} catch (IpmiException e) {
			result = ResultData.createFail(e);
		}

		return result;
	}

}
