package mark.ipmi.web.markup;

import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation which marks methods to be exported to some markup structures (like
 * XML, JSON, etc.). Concrete markup depends on MarkupBuilderIF implementation.
 * 
 * @author mark
 * 
 */
@Target(METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Export {

	/**
	 * Specifies name of the property in output markup
	 * 
	 * @return
	 */
	String name();

	/**
	 * Specifies formatter class, which instance will be used to format property
	 * prior to outputting it to result markup
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes" })
	Class formatterClass() default Object.class;

	/**
	 * Allows to specify a list of properties of this property, which are only
	 * allowed to be exported. Empty list means all properties are allowed.
	 * 
	 * @return
	 */
	String[] exportFilter() default {};

}
