package mark.ipmi.web.server;

/**
 * Defines methods for any web server implementation
 * 
 * @author mark
 * 
 */
public interface WebServerIF {

	public void start() throws Exception;

}
