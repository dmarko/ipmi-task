package mark.ipmi.web.vertx.server;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class ChassisRestVerticle extends Verticle {

	public ChassisRestVerticle() {

	}

	@Override
	public void start() {
		HttpServer server = vertx.createHttpServer();

		JsonObject conf = getContainer().config();
		final int port = conf.getInteger("port");

		RouteMatcher routeMatcher = new RouteMatcher();
		routeMatcher.get("/ipmi/chassis/", new Handler<HttpServerRequest>() {
			public void handle(final HttpServerRequest req) {

				// send request to event bus in order to handle request
				// regarding chassis
				getVertx().eventBus().send("chassis", toCredentialsJson(req), new Handler<Message<String>>() {
					@Override
					public void handle(Message<String> msg) {

						req.response().end(msg.body());

					}
				});

			}
		});
		routeMatcher.get("/ipmi/chassis/power/", new Handler<HttpServerRequest>() {
			public void handle(final HttpServerRequest req) {

				// send request to event bus in order to handle request
				// regarding chassis
				
				JsonObject params = toCredentialsJson(req);
				params.putString("on", req.params().get("on"));
				
				getVertx().eventBus().send("chassis/power", params, new Handler<Message<String>>() {
					@Override
					public void handle(Message<String> msg) {

						req.response().end(msg.body());

					}
				});

			}
		});
		routeMatcher.get("/ipmi/chassis/power/reset/", new Handler<HttpServerRequest>() {
			public void handle(final HttpServerRequest req) {

				// send request to event bus in order to handle request
				// regarding chassis
				
				JsonObject params = toCredentialsJson(req);
				params.putString("activated", req.params().get("activated"));
				
				getVertx().eventBus().send("chassis/power/reset", params, new Handler<Message<String>>() {
					@Override
					public void handle(Message<String> msg) {

						req.response().end(msg.body());

					}
				});

			}
		});
		server.requestHandler(routeMatcher).listen(port, "localhost");

	}

	private static JsonObject toCredentialsJson(final HttpServerRequest request) {

		JsonObject jsonObject = new JsonObject();
		jsonObject.putString("password", request.params().get("password"));
		jsonObject.putString("login", request.params().get("login"));
		jsonObject.putString("host", request.params().get("host"));
		return jsonObject;
	}
	
	
	
	

}
