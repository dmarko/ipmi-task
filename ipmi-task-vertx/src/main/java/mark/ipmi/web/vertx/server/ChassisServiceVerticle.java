package mark.ipmi.web.vertx.server;

import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.verax.VeraxIpmiManager;
import mark.ipmi.service.ChassisServiceIF;
import mark.ipmi.service.ResultData;
import mark.ipmi.vertx.service.BasicChassisService;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import com.google.gson.Gson;

public class ChassisServiceVerticle extends Verticle {

	private ChassisServiceIF chassisService;

	public ChassisServiceVerticle() {

		// TODO proper dependency injection
		VeraxIpmiManager ipmiManager = new VeraxIpmiManager();
		chassisService = new BasicChassisService(ipmiManager);
	}

	@Override
	public void start() {

		getVertx().eventBus().registerLocalHandler("chassis", new Handler<Message<JsonObject>>() {
			@Override
			public void handle(Message<JsonObject> event) {

				JsonObject ipmiCredentials = event.body();
				ResultData<ChassisIF> result = null;
				try {
					result = chassisService.getChassis(ipmiCredentials.getString("host"), ipmiCredentials.getString("login"), ipmiCredentials.getString("password"));
				} catch (Exception e) {
					// TODO ? decide later...
					e.printStackTrace();
				}

				event.reply(toJson(result));

			}
		});
		
		getVertx().eventBus().registerLocalHandler("chassis/power", new Handler<Message<JsonObject>>() {
			@Override
			public void handle(Message<JsonObject> event) {

				JsonObject params = event.body();
				ResultData<?> result = null;
				try {
					result = chassisService.switchPower(params.getString("on"), params.getString("host"), params.getString("login"), params.getString("password"));
				} catch (Exception e) {
					// TODO ? decide later...
					e.printStackTrace();
				}

				event.reply(toJson(result));

			}
		});
		
		getVertx().eventBus().registerLocalHandler("chassis/power/reset", new Handler<Message<JsonObject>>() {
			@Override
			public void handle(Message<JsonObject> event) {

				JsonObject params = event.body();
				ResultData<?> result = null;
				try {
					result = chassisService.resetPower(params.getString("activated"), params.getString("host"), params.getString("login"), params.getString("password"));
				} catch (Exception e) {
					// TODO ? decide later...
					e.printStackTrace();
				}

				event.reply(toJson(result));

			}
		});
		


	}

	private static String toJson(ResultData<?> rd) {

		return new Gson().toJson(rd);
	}

}
