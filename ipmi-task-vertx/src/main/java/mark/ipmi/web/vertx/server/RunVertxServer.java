package mark.ipmi.web.vertx.server;

import java.io.IOException;
import java.net.URL;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.PlatformLocator;
import org.vertx.java.platform.PlatformManager;

public class RunVertxServer {

	public RunVertxServer() {
	}

	public static void main(String[] args) throws IOException {
		PlatformManager pm = PlatformLocator.factory.createPlatformManager();

		JsonObject conf = new JsonObject().putNumber("port", Integer.valueOf("8999"));

		System.out.println(RunVertxServer.class.getClassLoader().getResource("./"));

		pm.deployVerticle(ChassisRestVerticle.class.getCanonicalName(), conf, new URL[] { RunVertxServer.class.getClassLoader().getResource("./") }, 1, null, new Handler<AsyncResult<String>>() {

			@Override
			public void handle(AsyncResult<String> event) {
				System.out.println(event.result());

			}
		});

		pm.deployWorkerVerticle(false, ChassisServiceVerticle.class.getCanonicalName(), conf, new URL[] { RunVertxServer.class.getClassLoader().getResource("./") }, 1, null, new Handler<AsyncResult<String>>() {

			@Override
			public void handle(AsyncResult<String> event) {
				System.out.println(event.result());

			}
		});

		System.in.read();

	}

}
