package mark.ipmi.domain.verax;

import static org.junit.Assert.*;
import mark.ipmi.domain.ChassisIF;
import mark.ipmi.domain.IpmiCredentialsIF;
import mark.ipmi.domain.IpmiException;
import mark.ipmi.domain.SelEntryIF;

import org.junit.Test;

public class VeraxIpmiManagerTest {

	VeraxIpmiManager ipmiManager = new VeraxIpmiManager();
	
	IpmiCredentialsIF credentials = new IpmiCredentialsIF() {
		
		@Override
		public String getPassword() {
			return "ADMIN";
		}
		
		@Override
		public String getLogin() {
			return "ADMIN";
		}
		
		@Override
		public String getHost() {
			return "msk.rsc-tech.ru";
		}
	};
	
	/**
	 * Check if we get the SEL entry with same id as requested
	 */
	@Test
	public void getSelEntry() {
		
		int expectedId = 1;
		
		SelEntryIF actualSelEntry = null;
		try {
			actualSelEntry = ipmiManager.getSelEntry(1, credentials);
		} catch (IpmiException e) {
			fail("should not thorw exception");
			e.printStackTrace();
		}
		
		assertNotNull(actualSelEntry);
		assertEquals(expectedId, actualSelEntry.getId());
		
	}
	
	/**
	 * Check if we just get the first SEL entry
	 */
	@Test
	public void getFirstSelEntry() {
		
		SelEntryIF actualSelEntry = null;
		try {
			actualSelEntry = ipmiManager.getFirstSelEntry(credentials);
		} catch (IpmiException e) {
			fail("should not thorw exception");
			e.printStackTrace();
		}
		
		assertNotNull(actualSelEntry);
		
	}
	
	/**
	 * Check if we just get the last SEL entry (getNext() should return null)
	 */
	@Test
	public void getLastSelEntry() {
		
		SelEntryIF actualSelEntry = null;
		try {
			actualSelEntry = ipmiManager.getLastSelEntry(credentials);
		} catch (IpmiException e) {
			fail("should not thorw exception");
			e.printStackTrace();
		}
		
		assertNotNull(actualSelEntry);
		assertNull(actualSelEntry.getNext());
	}
	
	/**
	 * Check if power is switched
	 */
	@Test
	public void switchPower() {
		
		ChassisIF actualChassis = null;
		
		//first check power settings and detect power status
		try {
			actualChassis = ipmiManager.getChassis(credentials);
		} catch (IpmiException e) {
			fail("should not throw exception");
			e.printStackTrace();
		}
		
		boolean currentPowerSwitch = actualChassis.isPowerOn();
		boolean expectedPowerSwitch = !currentPowerSwitch;
		
		//next, try to switch power, and it should work
		try {
			ipmiManager.switchPower(expectedPowerSwitch, credentials);
		} catch (IpmiException e) {
			fail("should not throw exception");
			e.printStackTrace();
		}
		
		//check if power has switched
		try {
			actualChassis = ipmiManager.getChassis(credentials);
		} catch (IpmiException e) {
			fail("should not throw exception");
			e.printStackTrace();
		}
		assertEquals(expectedPowerSwitch, actualChassis.isPowerOn());
		
		//check that it is not allowed to switch power to same setting (should throw exception)
		IpmiException ipmiException = null;
		try {
			ipmiManager.switchPower(expectedPowerSwitch, credentials);
		} catch (IpmiException e) {
			ipmiException = e;
		}
		assertNotNull(ipmiException);
		
	}

}
